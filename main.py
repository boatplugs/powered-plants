import asyncio
from kasa import Discover
import datetime


def get_plant_plug():
    discovered = asyncio.run(Discover.discover())
    for addr, dev in discovered.items():
        if dev.alias == "iot_plants_light":
            return dev
        else:
            return None


async def turn_off(device):
    if device:
        if device.is_on:
            await device.turn_off()
            await device.update()
            return device.is_off
    else:
        return print("Failed to turn off plug")


async def turn_on(device):
    if device:
        if device.is_off:
            await device.turn_on()
            await device.update()
            return device.is_on
    else:
        return print("Failed to turn on plug")


def toggle_device(device):
    if device.is_off is True:
        asyncio.run(turn_on(device))
        print("{}: plant plug is on!".format(datetime.datetime.now()))
    elif device.is_on is True:
        print(device.on_since)
        asyncio.run(turn_off(device))
        print("{}: plant plug is off!".format(datetime.datetime.now()))


plant_plug = get_plant_plug()
toggle_device(plant_plug)
